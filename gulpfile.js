var gulp        = require('gulp'),
    babelify    = require('babelify'),
    browserify  = require('browserify'),
    config      = require('./config.json'),
    connect     = require('gulp-connect'),
    buffer      = require('vinyl-buffer'),
    concat      = require('gulp-concat'),
    cssmin      = require('gulp-cssmin'),
    del         = require('del'),
    gulpif      = require('gulp-if'),
    inject      = require('gulp-inject'),
    proxy       = require('http-proxy-middleware'),
    rename      = require('gulp-rename'),
    runSequence = require('run-sequence'),
    sass        = require('gulp-sass'),
    source      = require('vinyl-source-stream'),
    sourcemaps  = require('gulp-sourcemaps'),
    uglify      = require('gulp-uglify');

let deepAssign = require('object-assign-deep');
let arqv = require('minimist')(process.argv.slice(2));
console.dir(arqv);
let env = arqv.production ? 'production' : 'development';
console.log('env = ' + env);
config[env] = deepAssign({}, config.default, config[env]);

gulp.task('connect', function() {
  connect.server({
    name: config[env].server.name,
    root: ['.', config[env].server.root],
    port: config[env].server.port,
    middleware: function(connect, opt) {
      return [
        proxy('/api', {
          target: 'http://localhost:3003',
          changeOrigin: true,
          ws: true // <-- set it to 'true' to proxy WebSockets
        })
      ];
    },
    livereload: true
  });
});

gulp.task('clean', function() {
  return del(config[env].clean.path).then(paths => {
    console.log('Deleted files in folder:\n', paths.join('\n'));
  });
});

gulp.task('app-styles', function() {
  return gulp.src(config[env].src.styles)
    .pipe(gulpif(config[env].appStyles.concat, sass().on('error', sass.logError)))
    .pipe(gulpif(config[env].appStyles.minify, cssmin()))
    .pipe(gulpif(config[env].appStyles.minify, rename({suffix: '.min'})))
    .pipe(gulp.dest(config[env].dest.styles))
    .pipe(connect.reload());
});

gulp.task('app-scripts', function() {
  return browserify({
      entries: config[env].src.entry,
      debug: true
    })
    .transform('babelify', {
      presets: ["es2015"]
    })
    .bundle()
    .on('error', function(err) {
      console.log(err);
    })
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(gulpif(config[env].appScripts.minify, uglify()))
    .pipe(gulpif(config[env].appScripts.minify, rename({suffix: '.min'})))
    .pipe(sourcemaps.write('../js'))
    .pipe(gulp.dest(config[env].dest.js))
    .pipe(connect.reload());
});

gulp.task('vendor-styles', function() {
  return gulp.src(config[env].src.vendor_styles)
    .pipe(gulpif(config[env].vendorStyles.minify, cssmin()))
    .pipe(gulpif(config[env].vendorStyles.concat, concat(config[env].vendorStyles.output)))
    .pipe(gulp.dest(config[env].dest.styles));
});

gulp.task('vendor-scripts', function() {
  return gulp.src(config[env].src.vendor_js)
    .pipe(gulpif(config[env].vendorScripts.concat, concat(config[env].vendorScripts.output)))
    .pipe(gulpif(config[env].vendorScripts.minify, uglify()))
    .pipe(gulp.dest(config[env].dest.js));
});

gulp.task('index', function() {
  var target = gulp.src(config[env].src.html);
  var vendorJS = gulp.src(config[env].vendorScripts.injectPath, {
    read: false
  });
  var appJS = gulp.src(config[env].appScripts.injectPath, {
    read: false
  });
  var vendorStyles = gulp.src(config[env].vendorStyles.injectPath, {
    read: false
  });
  var appStyles = gulp.src(config[env].appStyles.injectPath, {
    read: false
  });

  return target.pipe(inject(vendorJS, { name: 'head', ignorePath: 'build/', addRootSlash: false }))
    .pipe(inject(appJS, { ignorePath: 'build/', addRootSlash: false}))
    .pipe(inject(vendorStyles, { name: 'head', ignorePath: 'build/', addRootSlash: false }))
    .pipe(inject(appStyles, { ignorePath: 'build/', addRootSlash: false }))
    .pipe(gulp.dest(config[env].dest.html))
    .pipe(connect.reload());
});

gulp.task('favicon', function() {
  return gulp.src(config[env].src.favicon)
    .pipe(gulp.dest(config[env].dest.favicon));
});

gulp.task('build', function(callback) {
  runSequence('clean', ['app-scripts', 'app-styles', 'vendor-scripts', 'vendor-styles', 'favicon'],
    'index',
    callback);
});

gulp.task('watch', function() {
  gulp.watch(config[env].watch.js, ['app-scripts']);
  gulp.watch(config[env].watch.styles, ['app-styles']);
  gulp.watch(config[env].watch.html, ['index']);
});

gulp.task('default', function(callback) {
  runSequence('build', 'connect', 'watch', callback);
  require('./server.js');
});