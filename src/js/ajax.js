import { elements } from './constants';

export default function readyContent(menuItem, callback) {
  $.ajax('/api/' + menuItem).then((res) => callback(res));
}