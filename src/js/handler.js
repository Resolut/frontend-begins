import {  tokenList,  elements } from './constants';
import readyContent from './ajax';

let activeIndex = 0;

function createPopupMessage(res) {
  let infoMessage = document.createElement("div");
  infoMessage.className = 'alert alert-success alert-dismissable fade in';
  infoMessage.innerHTML = res + 
    '<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>';
  elements[tokenList[activeIndex]].content.append(infoMessage);

  setTimeout(() => {  infoMessage.remove(); }, 3000);
}

export default function onMenuItemClick(newIndex) {
  elements[tokenList[activeIndex]].menu.removeClass('active')
    .addClass('not-pressed');
  elements[tokenList[activeIndex]].content.addClass('hide');
  elements[tokenList[newIndex]].menu.addClass('active')
    .removeClass('not-pressed');
  elements[tokenList[newIndex]].content.removeClass('hide');
  readyContent(tokenList[newIndex], createPopupMessage);

  activeIndex = newIndex;
}