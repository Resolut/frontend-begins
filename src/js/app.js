import { tokenList, elements } from './constants';
import onMenuItemClick from './handler';
import customHandler from './custom';

for (let i = 1; i <= tokenList.length - 1; i++) {
  elements[tokenList[i]].content.addClass('hide');
  elements[tokenList[i]].menu.addClass('not-pressed');
}

for (let i = 0; i < tokenList.length; i++) {
  elements[tokenList[i]].menu.click(() => { onMenuItemClick(i); });
}

customHandler();