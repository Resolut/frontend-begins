export const tokenList = ['home', 'search', 'mission', 'price', 'about'];
export const elements = {};

for (let i = 0; i < tokenList.length; i++) {
  elements[tokenList[i]] = ['menu', 'content'];
  elements[tokenList[i]].menu = $("#" + tokenList[i]);
  elements[tokenList[i]].content = $("#" + tokenList[i] + "Content");
}
