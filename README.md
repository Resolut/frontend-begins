# Frontend-Begin

This is project for studying Frontend technologies stacks.  
All experience, success, pain and issues as is.  
Studying platform is [**MKDEV.ME**](https://mkdev.me)  

## Список заданий
 + Задание 1: Основы распределенной работы, Git и Bitbucket  
 + Задание 2: Создание npm-проекта, package.json  
 + Задание 3: Автоматизированная сборка проекта, gulp.js  
 + Задание 4: Внешние зависимости на примере jQuery и Bootstrap  
 + Задание 5: Надстройки на примере SASS и ES6  
 + Задание 6: Сервер разработки, API-сервер  
 + Задание 7: Вопросы окружения  

## Author:

#### **Dmitriy Samofalov**

## Mentor:
#### **Denis Alexanov**


