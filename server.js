var express = require('express');
var app = express();

app.use(express.static('build'));
app.get('/api/home', function(req, res) {
  res.send('Home API works properly...');
});
app.get('/api/search', function(req, res) {
  res.send('Search API works properly...');
});
app.get('/api/mission', function(req, res) {
  res.send('Mission API works properly...');
});
app.get('/api/price', function(req, res) {
  res.send('Price API works properly...');
});
app.get('/api/about', function(req, res) {
  res.send('About API works properly...');
});

app.listen(process.env.PORT || 3003);